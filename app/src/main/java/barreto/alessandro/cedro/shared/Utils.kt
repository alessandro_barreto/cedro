package barreto.alessandro.cedro.shared

import android.content.Context
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.View
import android.widget.Toast
import barreto.alessandro.cedro.feature.entries.ListEntriesActivity
import barreto.alessandro.cedro.feature.login.LoginActivity
import com.facebook.android.crypto.keychain.AndroidConceal
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain
import com.facebook.crypto.CryptoConfig
import com.facebook.crypto.Entity
import com.facebook.crypto.exception.CryptoInitializationException
import com.facebook.crypto.exception.KeyChainException
import net.idik.lib.cipher.so.CipherClient
import org.json.JSONObject
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.regex.Matcher
import java.util.regex.Pattern


class Utils {

    companion object {

        fun getTokenFromResponse(response: String): String {
            val json = JSONObject(response)
            return json.getString(Constants.TOKEN)
        }

        fun isValidPassword(password: String): Boolean {
            val pattern: Pattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{10,}$")
            val matcher: Matcher
            matcher = pattern.matcher(password)
            return matcher.matches()
        }

        fun gotToEntries(context: AppCompatActivity) {
            context.startActivity(Intent(context, ListEntriesActivity::class.java))
            context.finish()
        }

        fun goToLogin(context: AppCompatActivity) {
            context.startActivity(Intent(context, LoginActivity::class.java))
            context.finish()
        }

        fun showSnackbar(view: View, idString: Int?) {
            Snackbar.make(view, idString!!, Snackbar.LENGTH_LONG).show()
        }

        fun showToast(context: Context, resId: Int?) {
            Toast.makeText(context, resId!!, Toast.LENGTH_SHORT).show()
        }

        @Throws(KeyChainException::class, CryptoInitializationException::class, IOException::class)
        fun encrypt(context: Context, value: String): String {
            val keyChain = SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256)
            val crypto = AndroidConceal.get().createDefaultCrypto(keyChain)
            val bout = ByteArrayOutputStream()
            val cryptoStream = crypto.getCipherOutputStream(bout, Entity.create(CipherClient.keyFiedlBd()))
            cryptoStream.write(value.toByteArray(charset("UTF-8")))
            cryptoStream.close()
            val result = Base64.encodeToString(bout.toByteArray(), Base64.DEFAULT)
            bout.close()
            return result
        }

        @Throws(KeyChainException::class, CryptoInitializationException::class, IOException::class)
        fun decrypt(context: Context, value: String): String {
            val keyChain = SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256)
            val crypto = AndroidConceal.get().createDefaultCrypto(keyChain)
            val bin = ByteArrayInputStream(Base64.decode(value, Base64.DEFAULT))
            val cryptoStream = crypto.getCipherInputStream(bin, Entity.create(CipherClient.keyFiedlBd()))
            val bout = ByteArrayOutputStream()
            val buffer = ByteArray(1024)
            while (true){
                val read = cryptoStream.read(buffer)
                if (read == -1) break
                bout.write(buffer,0,read)
            }
            cryptoStream.close()
            val result = String(bout.toByteArray(), charset("UTF-8"))
            bin.close()
            bout.close()
            return result
        }

    }

}