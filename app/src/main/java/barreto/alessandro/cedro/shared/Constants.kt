package barreto.alessandro.cedro.shared

class Constants {

    companion object {
        const val TOKEN = "token"
        const val USER = "user"
        const val ENTRIES = "entries"
        const val ISEDIT = "is_edit"
    }

}