package barreto.alessandro.cedro.feature.entries

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.text.TextUtils
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.model.Entries
import barreto.alessandro.cedro.shared.Utils

class AddEntriesViewModel(application: Application) : AndroidViewModel(application) {

    val errorText = ObservableField<Int>()
    var siteField = ObservableField<String>()
    var userField = ObservableField<String>()
    var passwordField = ObservableField<String>()
    var id = ObservableField<Long>()
    var isEditMode = ObservableField<Boolean>(false)
    private val repository: EntriesRepository = EntriesRepository(application)

    fun saveEntries() {
        if (TextUtils.isEmpty(siteField.get()) || TextUtils.isEmpty(userField.get()) || TextUtils.isEmpty(passwordField.get())) {
            errorText.set(R.string.alert_fields_empty)
        } else {
            val entries = Entries(null,
                    siteField.get().toString(),
                    userField.get().toString(),
                    Utils.encrypt(getApplication(),passwordField.get().toString()),
                    siteField.get().toString())
            if (isEditMode.get()!!) {
                entries.id = id.get()
                repository.updateEntries(entries)
            } else {
                repository.insertEntries(entries)
            }
        }
    }

    fun deleteEntries(entries: Entries?) {
        repository.deleteEntries(entries!!)
    }

    fun addDataToEdit(entries: Entries?){
        siteField.set(entries?.siteUrl)
        userField.set(entries?.userSite)
        passwordField.set(entries?.password)
        id.set(entries?.id)
        isEditMode.set(true)
        passwordField.set(Utils.decrypt(getApplication(), entries!!.password))
    }

}