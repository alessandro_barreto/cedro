package barreto.alessandro.cedro.feature.entries

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.databinding.ActivityAddEntriesBinding
import barreto.alessandro.cedro.model.Entries
import barreto.alessandro.cedro.shared.Constants
import barreto.alessandro.cedro.shared.Utils

class AddEntriesActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityAddEntriesBinding
    private lateinit var viewModel: AddEntriesViewModel
    private var entries: Entries? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_entries)
        binding.btnSave.setOnClickListener(this)
        binding.btnDelete.setOnClickListener(this)
        viewModel = ViewModelProviders.of(this).get(AddEntriesViewModel::class.java)
        viewModel.errorText.addOnPropertyChangedCallback(callBackError)
        binding.viewModel = viewModel
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onStart() {
        super.onStart()
        val isEdit = intent.getBooleanExtra(Constants.ISEDIT, false)
        if (isEdit) {
            entries = intent.getParcelableExtra(Constants.ENTRIES)
            viewModel.addDataToEdit(entries)
        }
    }

    private val callBackError = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            Utils.showSnackbar(binding.container, viewModel.errorText.get())
        }
    }

    override fun onClick(view: View?) {
        when {
            view?.id == R.id.btn_save -> {
                viewModel.saveEntries()
                Utils.showToast(this@AddEntriesActivity, R.string.add_entries_ok)
            }
            view?.id == R.id.btn_delete -> {
                viewModel.deleteEntries(entries)
                Utils.showToast(this@AddEntriesActivity, R.string.delete_entries_ok)
            }
        }
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

}