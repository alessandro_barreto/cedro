package barreto.alessandro.cedro.feature.entries

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.databinding.ItemListEntriesBinding
import barreto.alessandro.cedro.model.Entries

class AdapterListEntries(val itemClick: (Entries) -> Unit) : RecyclerView.Adapter<AdapterListEntries.ViewHolder>() {

    private var listEntries: MutableList<Entries> = ArrayList()

    fun setListEntries(list: MutableList<Entries>) {
        this.listEntries = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemListEntriesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_list_entries, parent, false)
        return ViewHolder(binding,itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listEntries[position])
    }

    override fun getItemCount(): Int = listEntries.size

    inner class ViewHolder(val binding: ItemListEntriesBinding, val itemClick: (Entries) -> Unit) : RecyclerView.ViewHolder(binding.root) {
        fun bind(entries: Entries) {
            with(entries){
                binding.root.setOnClickListener { itemClick(this) }
            }
            binding.entries = entries
            binding.executePendingBindings()
        }
    }

}