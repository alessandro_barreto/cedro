package barreto.alessandro.cedro.feature.entries

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import barreto.alessandro.cedro.model.Entries

@Dao
interface EntriesDao {

    @Query("SELECT * FROM entries")
    fun getAll(): LiveData<List<Entries>>

    @Insert(onConflict = REPLACE)
    fun insert(entries: Entries)

    @Update
    fun update(entries: Entries)

    @Delete
    fun delete(entries: Entries)

}