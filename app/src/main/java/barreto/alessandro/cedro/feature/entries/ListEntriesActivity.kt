package barreto.alessandro.cedro.feature.entries

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.databinding.ActivityListEntriesBinding
import barreto.alessandro.cedro.model.Entries
import barreto.alessandro.cedro.shared.Constants


class ListEntriesActivity : AppCompatActivity(), (Entries) -> Unit {

    lateinit var binding: ActivityListEntriesBinding
    lateinit var viewModel: EntriesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_entries)
        viewModel = ViewModelProviders.of(this).get(EntriesViewModel::class.java)
        binding.viewModel = viewModel
        binding.rvlist.adapter = AdapterListEntries(this)
    }

    override fun onStart() {
        super.onStart()
        viewModel.entriesList.observe(this, Observer { list -> viewModel.bindingListEntries(list) })
    }

    fun clickAddEntries(view: View) {
        startActivity(Intent(this@ListEntriesActivity, AddEntriesActivity::class.java))
    }

    //Click item list
    override fun invoke(entries: Entries) {
        val intent = Intent(this@ListEntriesActivity, AddEntriesActivity::class.java)
        intent.putExtra(Constants.ISEDIT, true)
        intent.putExtra(Constants.ENTRIES, entries)
        startActivity(intent)
    }

}