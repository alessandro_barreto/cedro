package barreto.alessandro.cedro.feature.entries

import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import barreto.alessandro.cedro.model.Entries
import barreto.alessandro.cedro.shared.Constants
import barreto.alessandro.cedro.shared.Utils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.orhanobut.hawk.Hawk


class EntriesBindingAdapter {

    companion object {

        @JvmStatic
        @BindingAdapter("imageSite")
        fun adapter(imageView: ImageView, urlSite: String) {
            val glideUrl = GlideUrl("https://dev.people.com.ai/mobile/api/v2/logo/$urlSite", LazyHeaders.Builder()
                    .addHeader("authorization", Hawk.get<String>(Constants.TOKEN))
                    .build())
            Glide.with(imageView.context).load(glideUrl).into(imageView)
        }

        @JvmStatic
        @BindingAdapter("listEntries")
        fun adapter(recyclerView: RecyclerView, list: ObservableArrayList<Entries>) {
            (recyclerView.adapter as AdapterListEntries).setListEntries(list)
        }

        @JvmStatic
        @BindingAdapter("llAlert")
        fun adapter(linearLayout: LinearLayout, list: ObservableArrayList<Entries>) {
            if (list.isEmpty()) {
                linearLayout.visibility = View.VISIBLE
            } else {
                linearLayout.visibility = View.GONE
            }
        }

    }

}