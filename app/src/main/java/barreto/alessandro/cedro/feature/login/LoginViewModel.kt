package barreto.alessandro.cedro.feature.login

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.api.ApiService
import barreto.alessandro.cedro.model.User
import barreto.alessandro.cedro.shared.Constants
import barreto.alessandro.cedro.shared.Utils.Companion.getTokenFromResponse
import barreto.alessandro.cedro.shared.Utils.Companion.isValidPassword
import com.marcoscg.fingerauth.FingerAuth
import com.marcoscg.fingerauth.FingerAuthDialog
import com.orhanobut.hawk.Hawk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class LoginViewModel {

    val password = ObservableField<String>()
    val errorText = ObservableField<Int>()
    val isResponseSuccess = ObservableBoolean(false)
    val isLoading = ObservableBoolean(false)

    fun clickLoginButton() {

        when {
            TextUtils.isEmpty(password.get()) -> {
                errorText.set(R.string.alert_password_empty)
            }
            isValidPassword(password.get().toString()) -> {
                var user = Hawk.get<User>(Constants.USER)
                if (user.password.equals(password.get())){
                    loginUser(user)
                }else{
                    errorText.set(R.string.alert_password_incorrect)
                }
            }
            else -> {
                errorText.set(R.string.alert_password)
            }
        }

    }

    fun dialogFingerPrint(context: AppCompatActivity) {
        FingerAuthDialog(context)
                .setTitle(context.getString(R.string.login))
                .setCancelable(false)
                .setMaxFailedCount(3)
                .setNegativeButton(R.string.alert_cancel, { dialog, _ -> dialog.dismiss() })
                .setPositiveButton(R.string.login_password) { dialog, _ -> dialog.dismiss() }
                .setOnFingerAuthListener(object : FingerAuth.OnFingerAuthListener {
                    override fun onSuccess() {
                        loginUser(Hawk.get<User>(Constants.USER))
                    }

                    override fun onFailure() {
                        errorText.set(R.string.alert_finger_not_recognized)
                    }

                    override fun onError() {
                        errorText.set(R.string.alert_finger_not_recognized)
                    }
                })
                .show()
    }


    private fun loginUser(user: User) {
        isLoading.set(true)
        ApiService.service.login(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Hawk.put(Constants.TOKEN, getTokenFromResponse(response.string()))
                    isLoading.set(false)
                    isResponseSuccess.set(true)
                }, { e ->
                    isLoading.set(false)
                    errorText.set(R.string.alert_error_unknown)
                    e.printStackTrace()
                })
    }

}