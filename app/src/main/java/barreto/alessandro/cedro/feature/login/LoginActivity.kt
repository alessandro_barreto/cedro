package barreto.alessandro.cedro.feature.login

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.databinding.ActivityLoginBinding
import barreto.alessandro.cedro.shared.Utils

class LoginActivity : AppCompatActivity() {

    lateinit var binding : ActivityLoginBinding
    lateinit var viewModel : LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login)
        viewModel = LoginViewModel()
        viewModel.errorText.addOnPropertyChangedCallback(callBackError)
        viewModel.isResponseSuccess.addOnPropertyChangedCallback(callBackResponseSuccess)
        viewModel.dialogFingerPrint(this)
        binding.viewModel = viewModel
    }

    private val callBackError = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            Utils.showSnackbar(binding.container,viewModel.errorText.get())
        }
    }

    private val callBackResponseSuccess = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            if ((observable as ObservableBoolean).get()) {
                Utils.gotToEntries(this@LoginActivity)
            }
        }
    }

}