package barreto.alessandro.cedro.feature.sign_up

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.databinding.ActivitySignUpBinding
import barreto.alessandro.cedro.shared.Constants
import barreto.alessandro.cedro.shared.Utils
import com.orhanobut.hawk.Hawk


class SignUpActivity : AppCompatActivity() {

    lateinit var binding: ActivitySignUpBinding
    lateinit var viewModel: SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)

        if (Hawk.contains(Constants.TOKEN)) {
            Utils.goToLogin(this@SignUpActivity)
            return
        }

        viewModel = SignUpViewModel()
        viewModel.errorText.addOnPropertyChangedCallback(callBackError)
        viewModel.isResponseSuccess.addOnPropertyChangedCallback(callBackResponseSuccess)
        binding.viewModel = viewModel
    }

    private val callBackError = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            Utils.showSnackbar(binding.container,viewModel.errorText.get())
        }
    }

    private val callBackResponseSuccess = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            if ((observable as ObservableBoolean).get()) {
                Utils.gotToEntries(this@SignUpActivity)
            }
        }
    }

}