package barreto.alessandro.cedro.feature.entries

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.databinding.ObservableArrayList
import barreto.alessandro.cedro.model.Entries


class EntriesViewModel(application: Application) : AndroidViewModel(application) {

    var list = ObservableArrayList<Entries>()
    private val repository: EntriesRepository = EntriesRepository(application)
    val entriesList: LiveData<List<Entries>> = repository.getAllEntries()

    fun bindingListEntries(data: List<Entries>?) {
        if (data != null) {
            list.clear()
            list.addAll(data)
        }
    }

}