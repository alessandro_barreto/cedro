package barreto.alessandro.cedro.feature.entries

import android.app.Application
import android.arch.lifecycle.LiveData
import barreto.alessandro.cedro.database.AppDatabase
import barreto.alessandro.cedro.model.Entries

class EntriesRepository(application: Application) {
    private lateinit var entriesDao: EntriesDao
    private lateinit var allEntries: LiveData<List<Entries>>

    init {
        val database = AppDatabase.getDatabase(application)
        if (database != null) {
            entriesDao = database.entriesDao()
            allEntries = entriesDao.getAll()
        }
    }

    fun getAllEntries(): LiveData<List<Entries>> {
        return allEntries
    }

    fun insertEntries(entries: Entries) {
        entriesDao.insert(entries)
    }

    fun updateEntries(entries: Entries) {
        entriesDao.update(entries)
    }

    fun deleteEntries(entries: Entries) {
        entriesDao.delete(entries)
    }

}