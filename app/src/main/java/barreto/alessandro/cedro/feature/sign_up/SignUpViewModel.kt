package barreto.alessandro.cedro.feature.sign_up

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.text.TextUtils
import barreto.alessandro.cedro.R
import barreto.alessandro.cedro.api.ApiService
import barreto.alessandro.cedro.model.User
import barreto.alessandro.cedro.shared.Constants
import barreto.alessandro.cedro.shared.Utils
import barreto.alessandro.cedro.shared.Utils.Companion.getTokenFromResponse
import com.orhanobut.hawk.Hawk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SignUpViewModel {

    val name = ObservableField<String>()
    val email = ObservableField<String>()
    val password = ObservableField<String>()
    val errorText = ObservableField<Int>()
    val isResponseSuccess = ObservableBoolean(false)
    val isLoading = ObservableBoolean(false)

    fun clickSignUpButton() {
        if (TextUtils.isEmpty(name.get()) || TextUtils.isEmpty(email.get()) || TextUtils.isEmpty(password.get())) {
            errorText.set(R.string.alert_fields_empty)
        } else if (Utils.isValidPassword(password.get().toString())) {
            registerUser(User(name.get(), email.get(), password.get()))
        } else {
            errorText.set(R.string.alert_password)
        }

    }

    private fun registerUser(user: User) {
        isLoading.set(true)

        ApiService.service.register(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    isLoading.set(false)
                    Hawk.put(Constants.USER, user)
                    Hawk.put(Constants.TOKEN, getTokenFromResponse(response.string()))
                    isResponseSuccess.set(true)
                }, { e ->
                    isLoading.set(false)
                    errorText.set(R.string.alert_error_unknown)
                    e.printStackTrace() //TODO
                })
    }

}