package barreto.alessandro.cedro.services

import barreto.alessandro.cedro.model.User
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PeopleService{

    @POST("register")
    fun register(@Body user:User) : Observable<ResponseBody>

    @POST("login")
    fun login(@Body user:User) : Observable<ResponseBody>

}