package barreto.alessandro.cedro.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "entries")
data class Entries(
        @PrimaryKey(autoGenerate = true) var id: Long? = null,
        @ColumnInfo(name = "site_url") var siteUrl: String,
        @ColumnInfo(name = "user_site") var userSite: String,
        @ColumnInfo(name = "password") var password: String,
        @ColumnInfo(name = "site_url_image") var siteUrlImage: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(id)
        writeString(siteUrl)
        writeString(userSite)
        writeString(password)
        writeString(siteUrlImage)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Entries> = object : Parcelable.Creator<Entries> {
            override fun createFromParcel(source: Parcel): Entries = Entries(source)
            override fun newArray(size: Int): Array<Entries?> = arrayOfNulls(size)
        }
    }
}