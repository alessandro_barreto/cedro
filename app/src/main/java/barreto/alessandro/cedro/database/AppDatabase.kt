package barreto.alessandro.cedro.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import barreto.alessandro.cedro.feature.entries.EntriesDao
import barreto.alessandro.cedro.model.Entries

@Database(entities = arrayOf(Entries::class),version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun entriesDao(): EntriesDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AppDatabase::class.java, "cedro_database")
                                .allowMainThreadQueries()
                                .build()

                    }
                }
            }
            return INSTANCE
        }
    }

}