package barreto.alessandro.cedro


import android.app.Activity
import android.app.Instrumentation
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import barreto.alessandro.cedro.feature.entries.AddEntriesActivity
import barreto.alessandro.cedro.feature.entries.ListEntriesActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ListEntriesTest {

    @get:Rule
    var mActivityRule = ActivityTestRule(ListEntriesActivity::class.java, false, true)

    @Test
    fun whenActivityIsLaunched_shouldContainerViewListEmpty() {
        onView(withText(R.string.alert_title_empty)).check(matches(isDisplayed()))
    }

    @Test
    fun whenClickButtonAddEntries_shouldOpenAddEntriesActivity() {
        Intents.init()

        val matcher = IntentMatchers.hasComponent(AddEntriesActivity::class.java.name)
        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, null)
        Intents.intending(matcher).respondWith(result)

        onView(withId(R.id.floating_action_button)).perform(click())

        Intents.intended(matcher)
        Intents.release()

    }


}