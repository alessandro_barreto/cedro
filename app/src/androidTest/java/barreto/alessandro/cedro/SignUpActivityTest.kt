package barreto.alessandro.cedro

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import barreto.alessandro.cedro.api.ApiService
import barreto.alessandro.cedro.feature.entries.ListEntriesActivity
import barreto.alessandro.cedro.feature.sign_up.SignUpActivity
import barreto.alessandro.cedro.services.PeopleService
import com.google.gson.GsonBuilder
import com.orhanobut.hawk.Hawk
import net.vidageek.mirror.dsl.Mirror
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class SignUpActivityTest {

    private lateinit var server: MockWebServer

    @get:Rule
    var mActivityRule = ActivityTestRule(SignUpActivity::class.java, false, true)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        Hawk.init(mActivityRule.activity).build()
        server = MockWebServer()
        server.start()
        setupServerUrl()
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        Hawk.deleteAll()
        server.shutdown()
    }

    @Test
    fun whenActivityIsLaunched_shouldDisplayInitialState() {
        onView(withId(R.id.ed_name)).check(matches(isDisplayed()))
        onView(withId(R.id.ed_email)).check(matches(isDisplayed()))
        onView(withId(R.id.ed_password)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_enter)).check(matches(isDisplayed()))
    }

    @Test
    fun whenNameIsEmpty_andClickOnEnterButton_shouldDisplaySnackBar() {
        onView(withId(R.id.ed_email)).perform(typeText("ale@test.com"), closeSoftKeyboard())
        onView(withId(R.id.btn_enter)).perform(click())
        onView(withText(R.string.alert_fields_empty)).check(matches(isDisplayed()))
    }

    @Test
    fun whenPassWordIsEmpty_andClickOnEnterButton_shouldDisplaySnackBar() {
        onView(withId(R.id.ed_name)).perform(typeText("alessandro"), closeSoftKeyboard())
        onView(withId(R.id.btn_enter)).perform(click())
        onView(withText(R.string.alert_fields_empty)).check(matches(isDisplayed()))
    }

    @Test
    fun whenPassWordIsNotValidated_andClickEnterButton_shouldDisplaySnackBar() {
        onView(withId(R.id.ed_name)).perform(typeText("alessandro"), closeSoftKeyboard())
        onView(withId(R.id.ed_email)).perform(typeText("ale@test.com"), closeSoftKeyboard())
        onView(withId(R.id.ed_password)).perform(typeText("1234"), closeSoftKeyboard())
        onView(withId(R.id.btn_enter)).perform(click())
        onView(withText(R.string.alert_password)).check(matches(isDisplayed()))
    }

    @Test
    fun whenServerIsNotReponse_andClicEnterkButton_shouldDisplaySnackBar(){
        server.enqueue(MockResponse().setResponseCode(500))
        onView(withId(R.id.ed_name)).perform(typeText("alessandro"), closeSoftKeyboard())
        onView(withId(R.id.ed_email)).perform(typeText("ale@test.com"), closeSoftKeyboard())
        onView(withId(R.id.ed_password)).perform(typeText("Ale@#101010"), closeSoftKeyboard())
        onView(withId(R.id.btn_enter)).perform(click())
        onView(withText(R.string.alert_error_unknown)).check(matches(isDisplayed()))
    }

    @Test
    fun whenSignUpIsValid_andClickEnterButton_shouldOpenListEntriesActivity() {
        Intents.init()
        server.enqueue(MockResponse().setResponseCode(200).setBody("{\"type\":\"sucess\",\"token\":\"b64a150b-9506-40c4-aa37-be22d33dc053\"}"))
        onView(withId(R.id.ed_name)).perform(typeText("alessandro"), closeSoftKeyboard())
        onView(withId(R.id.ed_email)).perform(typeText("ale@test.com"), closeSoftKeyboard())
        onView(withId(R.id.ed_password)).perform(typeText("Ale@#101010"), closeSoftKeyboard())

        val matcher = hasComponent(ListEntriesActivity::class.java.name)
        val result = ActivityResult(Activity.RESULT_OK, null)
        intending(matcher).respondWith(result)

        onView(withId(R.id.btn_enter)).perform(click())

        intended(matcher)
        Intents.release()
    }

    private fun setupServerUrl() {
        val url = server.url("/").toString()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val gson = GsonBuilder().setLenient().create()

        val retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build()
                .create<PeopleService>(PeopleService::class.java)

        setField(ApiService, "service", retrofit)
    }

    private fun setField(target: Any, fieldName: String, value: Any) {
        Mirror().on(target).set().field(fieldName).withValue(value)
    }


}